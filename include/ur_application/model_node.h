#include <ros/ros.h>
#include "umrob/robot_model.h"
#include <std_msgs/Float64MultiArray.h>
#include <ur_application/Traj.h>
#include <eigen_conversions/eigen_msg.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fstream>
#include <sstream>
#include <filesystem>

class ServeAndPublish
{
public:
  ServeAndPublish()
  {
    serv_ = n_.advertiseService("Traj", &ServeAndPublish::handle_function, this);
    pub_ = n_.advertise<std_msgs::Float64MultiArray>("/left_arm_controller/command", 1);
    pub_.publish(right_cmd);
  }
  bool handle_function(ur_application::Traj::Request &req, ur_application::Traj::Response &res); 
private:
  ros::NodeHandle n_; 
  ros::Publisher pub_;
  ros::ServiceServer serv_;
  std_msgs::Float64MultiArray right_cmd;
  //right_cmd.data.resize(6, 0.);
};