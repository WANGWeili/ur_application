#include "ur_application/model_node.h"
#include <iostream>


  /*  Code Here  */
  /* robot_control */
  //                      req.target_position -> |Control| -> [theta1, theta2, theta3, ...].transpose()

  /* robot_model */
  /*
  auto binary_path = (std::filesystem::path(argv[0]).parent_path()).parent_path().parent_path();
    auto model_path =
        binary_path /
        std::filesystem::path("share/ur_application/urdf/dual_arm.urdf");
  std::ifstream model_file(model_path.generic_string());
  */
  /* Better way to read urdf while using .launch*/
  /*
  std::string model_path;
  ros::param::get("robot_description", model_path);
  std::ifstream model_file(model_path);
  if (not model_file.is_open()) {
      fmt::print(stderr, "Failed to open the URDF file\n");
      //std::cout << nh.hasParam("robot_description") << std::endl;
      std::exit(-1);
  }
  std::string urdf((std::istreambuf_iterator<char>(model_file)),
                    std::istreambuf_iterator<char>());

  auto model = umrob::RobotModel(urdf);*/

int main(int argc, char* argv[])
{
  // This must be called before anything else ROS-related
  ros::init(argc, argv, "model_node");

  ServeAndPublish SandP;
  //auto serv_ = SandP.n_.advertiseService("Traj", SandP.handle_function);
  
  // Don't exit the program.
  ros::spin();
}


bool ServeAndPublish::handle_function(ur_application::Traj::Request &req, ur_application::Traj::Response &res) 
{
  std::string model_path;
  ros::param::get("robot_description", model_path);
  std::ifstream model_file(model_path);
  if (not model_file.is_open()) {
      fmt::print(stderr, "Failed to open the URDF file\n");
      //std::cout << nh.hasParam("robot_description") << std::endl;
      std::exit(-1);
  }
  std::string urdf((std::istreambuf_iterator<char>(model_file)),
                    std::istreambuf_iterator<char>());

  auto model = umrob::RobotModel(urdf);

  /****************************** interface for robot_control ******************************/
  auto tra_pos = req.target_position;
  /****************************** positions from robot_control ******************************/
  model.jointPosition("right_shoulder_pan_joint")  = 0.0;           // = theta1 
  model.jointPosition("right_shoulder_lift_joint") = 0.0;           // = theta2
  model.jointPosition("right_elbow_joint")         = 0.0;           // = theta3
  model.jointPosition("right_wrist_1_joint")       = 0.0;           // = theta4
  model.jointPosition("right_wrist_2_joint")       = 0.0;           // = theta5
  model.jointPosition("right_wrist_3_joint")       = 0.0;           // = theta6
  model.jointPosition("left_shoulder_pan_joint")   = 0.0;           // = theta7
  model.jointPosition("left_shoulder_lift_joint")  = 0.0;           // = theta8
  model.jointPosition("left_elbow_joint")          = 0.0;           // = theta9
  model.jointPosition("left_wrist_1_joint")        = 0.0;           // = theta10
  model.jointPosition("left_wrist_2_joint")        = 0.0;           // = theta11
  model.jointPosition("left_wrist_3_joint")        = 0.0;           // = theta12

  std_msgs::Float64MultiArray right_theta;
  right_theta.data.resize(6, 0.);
  /****************************** velocity from robot_control ******************************/
  Eigen::VectorXd right_deltaTheta = Eigen::VectorXd::Ones(6);  // 6 = number of joints
  /******************************************************************************************/
  model.update();

  auto right_tool0 = model.linkPose("right_tool0");
  fmt::print("right_tool0 pose:\n{}\n", right_tool0.matrix());

  auto right_tool0_jacobian = model.linkJacobian("right_tool0");
  fmt::print("right_tool0 jacobian:\n{}\n",right_tool0_jacobian.matrix);

  auto left_tool0 = model.linkPose("left_tool0");
  fmt::print("left_tool0 pose:\n{}\n", left_tool0.matrix());

  auto left_tool0_jacobian = model.linkJacobian("right_tool0");
  fmt::print("left_tool0 jacobian:\n{}\n",left_tool0_jacobian.matrix);

  std_msgs::Float64MultiArray pre_pos;
  tf::matrixEigenToMsg(right_tool0.matrix(), pre_pos);
  res.present_position  = pre_pos; 

  //auto pinvOfJac = right_tool0_jacobian.matrix.pseudoInverse();
  auto joints_vel = right_tool0_jacobian.matrix * right_deltaTheta;
  std_msgs::Float64MultiArray pre_vel;
  tf::matrixEigenToMsg(joints_vel, pre_vel);
  res.present_velocity  = pre_vel;
  right_cmd.data = right_theta.data;

  return true;
}